(ns insane-noises.core
  (:require [overtone.live :refer :all])
  (:use [overtone.live]
        [overtone.inst.sampled-piano]
        [overtone.inst.drum]))

(def piano sampled-piano)
(def chords (list (note :C3)(chord :D3 :minor)(note :G3)(note :C3)(note :C3)(chord :D3 :minor)(note :G3)(note :C3)))

(defn note->chord [item]
  (if (seq? item) item (list item)))

(defn play-chord [a-chord]
  (doseq [note a-chord] (piano note)))


;(def dirty-kick (freesound 30669))

(let [time (now)]
  (at time (play-chord (chord :D3 :major)))
  (at (+ 2000 time) (play-chord (chord :A3 :major)))
  (at (+ 3000 time) (play-chord (chord :A3 :major7)))
  (at (+ 4300 time) (play-chord (chord :F3 :major7))))

(defn play-progression [chords]
  (if (empty? chords) nil
      (doseq []
        (play-chord (first chords))
        (Thread/sleep 200)
        (play-progression (rest chords)))))


(definst beep [note 60]
  (let [sound-src (sin-osc (midicps note))
        env (env-gen (perc 0.01 1.0) :action FREE)] ; sam uses :free
    (* sound-src env)))

(for [i (range 110)] (at (+ (now) (* i 20)) (beep (+ i))))

(definst plucked-string [note 60 amp 0.8 dur 2 decay 30 coef 0.3 gate 1]
  (let [freq (midicps note)
        noize (* 0.8 (white-noise))
        dly (/ 1.0 freq)
        plk (pluck noize gate dly dly decay coef)
        dist (distort plk)
        filt (rlpf dist (* 12 freq) 0.6)
        clp (clip2 filt 0.8)
        reverb (free-verb clp 0.4 0.8 0.2)]
    (* amp (env-gen (perc 0.0001 dur)) reverb)))

;; note: the underscores are rests
;(def reich-degrees [:vi :vii :i+ :_ :vii :_ :i+ :vii :vi :_ :vii :_])
(def reich-degrees [:vi :vii :i+ :_ :vii :_ :i+ :vii :vi :_ :vii :_])
(def pitches (degrees->pitches reich-degrees :ionian :C3))
(def pitches1 (degrees->pitches reich-degrees :dorian :C4))

(defn play
  [time notes sep]
  (let [note (first notes)]
    (when note
      (at time (plucked-string note)))
    (let [next-time (+ time sep)]
      (apply-by next-time play [next-time (rest notes) sep]))))

(defn play-piano
  [time notes sep]
  (let [note (first notes)]
    (when note
      (at time (piano note) (play-chord (chord :C4 :major))))
    (let [next-time (+ time sep)]
      (apply-by next-time play-piano [next-time (rest notes) sep]))))

(let [t (+ 800 (now))]
  (play-piano t (cycle pitches) 400))
;  (play-piano t (cycle pitches) 102)
;  (play-piano t (cycle pitches) 200)
; (play-piano t (cycle pitches) 400)

(stop)

(defn -main [& args]
  (play))
