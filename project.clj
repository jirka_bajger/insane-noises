(defproject insane-noises "0.0.1-SNAPSHOT"
  :description "insane-noises"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [overtone "0.9.1"]]
  :main insane-noises.core
  :plugins [[cider/cider-nrepl "0.9.0-SNAPSHOT"]]
)
